project_name = "Development"

DEBUG = True
LOG_LEVEL = 'DEBUG' # CRITICAL / ERROR / WARNING / INFO / DEBUG
LOG_DIR = '/usr/local/web/shared/working_directories/logs'

SERVER_NAME = '127.0.0.1:5001'
SECRET_KEY = '9sa8duf9safduds9afj'

UPLOAD = 'UPLOAD'
GOOGLE = 'GOOGLE'
WEB_TYPE = UPLOAD