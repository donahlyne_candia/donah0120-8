# README #

**Important 1: This project contains template in .md (markdown) format. Means this
readme and some of the template files are best viewed in a markdown viewer ... or ... the bitbucket website.**


**Important 2: This readme is meant as explanation how to use these templates. 
 It should not be the part of the project that you are creating from this template. 
 So when using this template, replace this readme with documentation of the actual project.**

This repository contains a template used for developing a big (multi) scraper project.

## How to use it ##

This template is meant to be used for big, multi scraper projects. Such
projects have their own repository.

### Steps for using this template: ###

1. copy all files in the main directory of the template to a new repository, also copy the folders ```deploy``` and ```web```
2. change into this new subdirectory
3. follow the [Steps for adding scrapy spiders] below to add as many scrapers as needed
4. adjust all the files listed under "Basic Elements" below
5. Cleanup:
    * replace the content of this README.md file with the setup_instructions_xx.md in ```docs``` (use the ..._en.md file unless told otherwise, also please copy the complete markdown sourcecode, not the text only version)
    * adjust the contents of the new README.md file to the project (e.g. remove sections that are not needed, adjust spider names etc ...)
    * remove the ```docs``` and ```helpful``` folders
    * remove any uneeded files


### Steps for adding scrapy spiders ###

For adding scrapers you need access to the ```internal_small_project_template``` which contains all scraper templates.

1. for adding a scrapy spider create a new subdirectory in ```./scrapers```. Name this subdirectory after the website the scraper will target, e.g. ```wikipedia_org```
2. change into this new scraper directory
3. initialize the scrapy project: ```scrapy startproject <project name> .``` (the "." is important for the scripts in this template to work)
4. generate the (first) spider: ```scrapy genspider <spider name> <domain>```
5. select and copy the template code that you need from the ```scrapy_template_files``` folder into the real files (please not that these are Markdown files for better readability, so only copy snippets):
    * [scrapy.cfg](scrapy_template_files/scrapy.cfg.md) - a template containing important snippet for the the main scrapy configuration file, needed for deploying to scrapyd servers
    * [settings.py](scrapy_template_files/settings.py.md) - a template containing various snippets for the settings file where all the fine tuning and proxy configuration is done
    * [spider.py](scrapy_template_files/spider.py.md) - various code templates for setting up a spider quickly
    * [items.py](scrapy_template_files/items.py.md) - various code templates for getting the items set up quickly
6. copy the following files in full from the ```scrapy_template_files/``` folder and
   adjust them as needed:
    * [test_spider.py](scrapy_template_files/test_spider.py) - a copy & paste unit test template for testing scrapy spiders ... copy it into the ```spider``` folder and rename it to ```test_<spider file name>.py```
    * [test_items.py](scrapy_template_files/test_items.py) - a copy & paste unit test template for testing scrapy items ... copy it next to your ```items.py```

## Basic Elements ##

This contains all the basic elements a new project should have:

* [.gitignore](.gitignore) - defines files and folders that
  usually should not be committed to git (like temporary files, data files,
  etc). If you need to commit such a file or folder to git nonetheless,
  use git's -f (force) option

* [requirements.txt](requirements.txt) - lists all python
  packages needed by this project. Through ```pip install -r requirements.txt```
  all required packages can be installed. Keeping this file complete
  is especially important for projects that are deployed to servers
   and/or delivered to clients

* [requirements_for_fabric.txt](requirements.txt) - a smaller version of 
  requirements.txt that contains only dependencies to run the fabric 
  deployment scripts.

* [fabfile.py](fabfile.py) - the fabric file for automatic
  deployment. If it is set up properly (besides the configurations in
  [deploy/config](deploy/config)), the whole project can
  be deployed automatically.

* [bitbucket-pipelines.yml](bitbucket-pipeline.yml) - configuration for the bitbucket pipeline
  that runs all tests automatically after each push

* [pytest.ini](pytest.ini) - configuration for pytest that automatically avoids
  running all the tests in venv/ where usually the various libraries are installed

* [deploy](deploy) - this folder is the place where deployment
  files for webservers etc are to be stored

  * [config](deploy/config) - this folder contains the various
    special configurations for deploying this project to different
    servers. That's what sometimes called "stages". The following list
    are default stages/configurations, but there can be more, as needed.

    * [production] - the production stage, as the name says, this is
      deploying and updating the productive system. As this is usually
      directly accessed by clients, this should be handled with extra
      care to avoid a) rolling out any untested and buggy code and b)
      to avoid unnecessary service interruptions for the customers

    * [beta] - the beta stage is for testing newest developments and
      sometimes showing it to the client before rolling everything out
      to the production stage

    * [development] - the stage for development. For big projects it
      might be a good idea to have a separate stage for each developer.

* [scrapers](scrapers) - the place where all scrapers will be located (in their own folders). It contains an example scraper that needs to be removed. The repository internal_small_project_template contains the templates for creating scrapers.

* [web](web) - the place where the web ui and/or web api implementation is located.


## TODO ##
* web app template should have different upload UI templates: single-file, 
multi-file, processed archive
* integrate fake

* add other setup templates and restructure them

Pipeline additions:
* automatically test deploy wiht bitbucket
* add simple tests to the pipeline that check the deployment result (e.g. testing for getting a 200 from the main page)
 